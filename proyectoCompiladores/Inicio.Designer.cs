﻿namespace proyectoCompiladores
{
    partial class Inicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonDesdeArchivo = new System.Windows.Forms.RadioButton();
            this.radioButtonPorConsola = new System.Windows.Forms.RadioButton();
            this.labelCargaDatos = new System.Windows.Forms.Label();
            this.textBoxConsola = new System.Windows.Forms.TextBox();
            this.openFileDialogCargaArchivo = new System.Windows.Forms.OpenFileDialog();
            this.buttonCargarArchivo = new System.Windows.Forms.Button();
            this.labelRutaArchivo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // radioButtonDesdeArchivo
            // 
            this.radioButtonDesdeArchivo.AutoSize = true;
            this.radioButtonDesdeArchivo.Location = new System.Drawing.Point(51, 81);
            this.radioButtonDesdeArchivo.Name = "radioButtonDesdeArchivo";
            this.radioButtonDesdeArchivo.Size = new System.Drawing.Size(94, 17);
            this.radioButtonDesdeArchivo.TabIndex = 0;
            this.radioButtonDesdeArchivo.TabStop = true;
            this.radioButtonDesdeArchivo.Text = "Desde archivo";
            this.radioButtonDesdeArchivo.UseVisualStyleBackColor = true;
            // 
            // radioButtonPorConsola
            // 
            this.radioButtonPorConsola.AutoSize = true;
            this.radioButtonPorConsola.Location = new System.Drawing.Point(342, 81);
            this.radioButtonPorConsola.Name = "radioButtonPorConsola";
            this.radioButtonPorConsola.Size = new System.Drawing.Size(81, 17);
            this.radioButtonPorConsola.TabIndex = 1;
            this.radioButtonPorConsola.TabStop = true;
            this.radioButtonPorConsola.Text = "Por consola";
            this.radioButtonPorConsola.UseVisualStyleBackColor = true;
            // 
            // labelCargaDatos
            // 
            this.labelCargaDatos.AutoSize = true;
            this.labelCargaDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCargaDatos.Location = new System.Drawing.Point(121, 19);
            this.labelCargaDatos.Name = "labelCargaDatos";
            this.labelCargaDatos.Size = new System.Drawing.Size(237, 37);
            this.labelCargaDatos.TabIndex = 2;
            this.labelCargaDatos.Text = "Carga de datos";
            this.labelCargaDatos.Click += new System.EventHandler(this.labelCargaDatos_Click);
            // 
            // textBoxConsola
            // 
            this.textBoxConsola.Location = new System.Drawing.Point(51, 163);
            this.textBoxConsola.Multiline = true;
            this.textBoxConsola.Name = "textBoxConsola";
            this.textBoxConsola.Size = new System.Drawing.Size(127, 95);
            this.textBoxConsola.TabIndex = 3;
            // 
            // openFileDialogCargaArchivo
            // 
            this.openFileDialogCargaArchivo.FileName = "CargaArchivo";
            this.openFileDialogCargaArchivo.Filter = "(*.txt)|*.txt";
            // 
            // buttonCargarArchivo
            // 
            this.buttonCargarArchivo.Location = new System.Drawing.Point(329, 122);
            this.buttonCargarArchivo.Name = "buttonCargarArchivo";
            this.buttonCargarArchivo.Size = new System.Drawing.Size(94, 28);
            this.buttonCargarArchivo.TabIndex = 4;
            this.buttonCargarArchivo.Text = "CargarArchivo";
            this.buttonCargarArchivo.UseVisualStyleBackColor = true;
            this.buttonCargarArchivo.Click += new System.EventHandler(this.buttonCargarArchivo_Click);
            // 
            // labelRutaArchivo
            // 
            this.labelRutaArchivo.AutoSize = true;
            this.labelRutaArchivo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRutaArchivo.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRutaArchivo.Location = new System.Drawing.Point(51, 124);
            this.labelRutaArchivo.Name = "labelRutaArchivo";
            this.labelRutaArchivo.Size = new System.Drawing.Size(212, 23);
            this.labelRutaArchivo.TabIndex = 5;
            this.labelRutaArchivo.Text = "No se ha cargado  archivo";
            this.labelRutaArchivo.Click += new System.EventHandler(this.labelRutaArchivo_Click);
            // 
            // Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 419);
            this.Controls.Add(this.labelRutaArchivo);
            this.Controls.Add(this.buttonCargarArchivo);
            this.Controls.Add(this.textBoxConsola);
            this.Controls.Add(this.labelCargaDatos);
            this.Controls.Add(this.radioButtonPorConsola);
            this.Controls.Add(this.radioButtonDesdeArchivo);
            this.Name = "Inicio";
            this.Text = "Carga de datos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonDesdeArchivo;
        private System.Windows.Forms.RadioButton radioButtonPorConsola;
        private System.Windows.Forms.Label labelCargaDatos;
        private System.Windows.Forms.TextBox textBoxConsola;
        private System.Windows.Forms.OpenFileDialog openFileDialogCargaArchivo;
        private System.Windows.Forms.Button buttonCargarArchivo;
        private System.Windows.Forms.Label labelRutaArchivo;
    }
}

